//
//  test_bankapp_ios_endcomApp.swift
//  test-bankapp-ios-endcom
//
//  Created by Abraham Rubio on 17/06/21.
//

import SwiftUI

@main
struct test_bankapp_ios_endcomApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
