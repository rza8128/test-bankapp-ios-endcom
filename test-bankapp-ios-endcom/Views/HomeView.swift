//
//  ContentView.swift
//  test-bankapp-ios-endcom
//
//  Created by Abraham Rubio on 17/06/21.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        NavigationView {
            VStack {
                /* Logo header */
                HStack {
                    Circle()
                        .stroke(Color.init(hex: "07BE92"), lineWidth: 5)
                        .frame(width: 25, height: 25)
                    Text("Bankapp")
                        .font(.title2)
                        .foregroundColor(Color.init(hex: "07BE92"))
                }
                .padding(.bottom, 5)
                ScrollView (showsIndicators: false) {
                    ScrollView (.horizontal, showsIndicators: false) {
                        HStack  {
                            Text("Octavio Martinez")
                                .bold()
                                .foregroundColor(.white)
                                .padding(.trailing, 10)
                            Text("Ultimo inicio 08/06/2021")
                                .bold()
                                .foregroundColor(Color.init(hex: "07BE92"))
                        }
                            
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: 30,
                        alignment: .leading
                    )
                    .padding()
                    .background(Color.init(hex: "01455C"))
                    HStack {
                        Text("\(Text("Mis Cuentas").underline().foregroundColor(Color.init(hex: "0592BD")))         \(Text("Enviar Dinero").underline().foregroundColor(Color.init(hex: "7B9093")))")
                            .bold()
                    }
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: 25,
                        alignment: .leading
                    )
                    .padding()
                    ScrollView (.horizontal, showsIndicators: false) {
                        HStack (spacing: 15) {
                            ZStack {
                                Rectangle()
                                    .foregroundColor(Color.init(hex: "01455C"))
                                    .frame(width: 250, height: 120)
                                    .cornerRadius(3.0)
                                Text("Saldo general\n en cuentas")
                                    .bold()
                                    .foregroundColor(Color.init(hex: "0592BD"))
                                    .frame(maxWidth: .infinity, maxHeight: .infinity ,alignment: .topLeading)
                                    .padding()
                                Text("$2,000.00   ")
                                    .bold()
                                    .font(.title2)
                                    .foregroundColor(Color.init(hex: "07BE92"))
                                    .frame(maxWidth: .infinity, maxHeight: .infinity ,alignment: .bottomTrailing)
                                    .padding()
                            }
                            ZStack {
                                Rectangle()
                                    .foregroundColor(Color.init(hex: "01455C"))
                                    .frame(width: 250, height: 120)
                                    .cornerRadius(3.0)
                                Text("Total de ingresos")
                                    .bold()
                                    .foregroundColor(Color.init(hex: "0592BD"))
                                    .frame(maxWidth: .infinity, maxHeight: .infinity ,alignment: .topLeading)
                                    .padding()
                                Text("$2,000.00   ")
                                    .bold()
                                    .font(.title2)
                                    .foregroundColor(Color.init(hex: "07BE92"))
                                    .frame(maxWidth: .infinity, maxHeight: .infinity ,alignment: .bottomTrailing)
                                    .padding()
                            }
                            ZStack {
                                Rectangle()
                                    .foregroundColor(Color.init(hex: "01455C"))
                                    .frame(width: 250, height: 120)
                                    .cornerRadius(3.0)
                                Text("Total de gastos")
                                    .bold()
                                    .foregroundColor(Color.init(hex: "0592BD"))
                                    .frame(maxWidth: .infinity, maxHeight: .infinity ,alignment: .topLeading)
                                    .padding()
                                Text("$2,000.00   ")
                                    .bold()
                                    .font(.title2)
                                    .foregroundColor(Color.init(hex: "07BE92"))
                                    .frame(maxWidth: .infinity, maxHeight: .infinity ,alignment: .bottomTrailing)
                                    .padding()
                            }
                        }.padding()
                    }
                    .padding()
                    .frame(width: .infinity, height: 120)
                    NavigationLink(destination: AddCardView()) {
                        Text("+ Agregar una tarjeta de débito o crédito")
                            .bold()
                            .underline()
                            .foregroundColor(Color.init(hex: "0592BD"))
                            .frame(
                                minWidth: 0,
                                maxWidth: .infinity,
                                minHeight: 0,
                                maxHeight: 20,
                                alignment: .leading
                            )
                            .padding()
                    }
                    VStack {
                        HStack (alignment: .top, spacing: 35) {
                            ZStack {
                                Rectangle()
                                    .foregroundColor(Color.init(hex: "1FC49D"))
                                    .frame(width: 60, height: 40)
                                    .cornerRadius(3.0)
                                HStack (spacing: -10) {
                                    Circle()
                                        .foregroundColor(Color.init(hex: "9A3B59"))
                                        .frame(width: 18, height: 18, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                    Circle()
                                        .foregroundColor(Color.init(hex: "D1D255"))
                                        .frame(width: 18, height: 18, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        .opacity(0.8)
                                }
                            }
                            .frame(
                                maxWidth: 60,
                                maxHeight: 120,
                                alignment: .topLeading
                            )
                            //.background(Color(.blue))
                            VStack (alignment: .leading, spacing: 10) {
                                HStack (alignment: .top) {
                                    Text("Activa")
                                        .bold()
                                        .font(.callout)
                                        .foregroundColor(Color.init(hex: "07BE92"))
                                    Spacer()
                                    Text("$1,500.00")
                                        .bold()
                                        .font(.title3)
                                        .foregroundColor(Color.init(hex: "01455C"))
                                    
                                }
                                Group {
                                    Text("5439 2401 1234 1234")
                                        .font(.callout)
                                        .bold()
                                    Text("Berenice Garcia Lopez")
                                        .font(.callout)
                                    Text("Titular")
                                        .font(.footnote)
                                }
                                .foregroundColor(Color.init(hex: "01455C"))
                            }
                            .frame(
                                maxWidth: 400,
                                maxHeight: 120,
                                alignment: .bottomLeading
                            )
                            
                            //.background(Color(.blue))
                        }
                        .padding(.bottom, 10)
                        HStack (alignment: .top, spacing: 35) {
                            ZStack {
                                Rectangle()
                                    .foregroundColor(Color.init(hex: "ABE5D6"))
                                    .frame(width: 60, height: 40)
                                    .cornerRadius(3.0)
                                HStack (spacing: -10) {
                                    Circle()
                                        .foregroundColor(Color.init(hex: "9A3B59"))
                                        .frame(width: 18, height: 18, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                    Circle()
                                        .foregroundColor(Color.init(hex: "D1D255"))
                                        .frame(width: 18, height: 18, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        .opacity(0.8)
                                }
                                
                            }
                            .frame(
                                maxWidth: 60,
                                maxHeight: 120,
                                alignment: .topLeading
                            )
                            //.background(Color(.blue))
                            VStack (alignment: .leading, spacing: 10) {
                                HStack {
                                    Text("Inactiva")
                                        .bold()
                                        .font(.callout)
                                        .foregroundColor(Color.init(hex: "07BE92"))
                                    Spacer()
                                    Text("$1,500.00")
                                        .bold()
                                        .font(.title3)
                                        .foregroundColor(Color.init(hex: "01455C"))
                                }
                                Group {
                                    Text("5439 2401 1234 1234")
                                        .font(.callout)
                                        .bold()
                                    Text("Berenice Garcia Lopez")
                                        .font(.callout)
                                    Text("Titular")
                                        .font(.footnote)
                                }
                                .foregroundColor(Color.init(hex: "01455C"))
                            }
                            .frame(
                                maxWidth: 400,
                                maxHeight: 120,
                                alignment: .bottomLeading
                            )
                            
                            //.background(Color(.blue))
                        }
                    }
                    .padding()
                    ZStack {
                        Rectangle()
                            .foregroundColor(Color.init(hex: "F5F5F5"))
                            .frame(width: .infinity)
                            .cornerRadius(5.0)
                            .shadow(color: .gray, radius: 2)
                        VStack (alignment: .leading, spacing: 10){
                            Text("Movimientos Recientes")
                                .bold()
                                .font(.title3)
                                .foregroundColor(Color.init(hex: "01455C"))
                            
                            
                            /* Agrupacion para movimientos*/
                            Group {
                                HStack {
                                    Text("UBER BV")
                                        .bold()
                                        .foregroundColor(Color.init(hex: "01455C"))
                                    Spacer()
                                    Text("-$250.00")
                                        .bold()
                                        .foregroundColor(Color.init(hex: "01455C"))
                                }
                                Text("10/05/2021")
                                    .font(.subheadline)
                                    .foregroundColor(Color.init(hex: "7B9093"))
                                Divider()
                                    .frame(maxWidth: .infinity, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            }
                            /* Agrupacion para movimientos*/
                            Group {
                                HStack {
                                    Text("UBER BV")
                                        .bold()
                                        .foregroundColor(Color.init(hex: "01455C"))
                                    Spacer()
                                    Text("-$250.00")
                                        .bold()
                                        .foregroundColor(Color.init(hex: "01455C"))
                                }
                                Text("10/05/2021")
                                    .font(.subheadline)
                                    .foregroundColor(Color.init(hex: "7B9093"))
                                Divider()
                                    .frame(maxWidth: .infinity, alignment: .center)
                            }
                            /* Agrupacion para movimientos*/
                            Group {
                                HStack {
                                    Text("UBER BV")
                                        .bold()
                                        .foregroundColor(Color.init(hex: "01455C"))
                                    Spacer()
                                    Text("-$250.00")
                                        .bold()
                                        .foregroundColor(Color.init(hex: "01455C"))
                                }
                                Text("10/05/2021")
                                    .font(.subheadline)
                                    .foregroundColor(Color.init(hex: "7B9093"))
                                Divider()
                                    .frame(maxWidth: .infinity, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            }
                            
                            HStack {
                                Image(systemName: "list.dash")
                                Text("Ver Todos los Moviemientos")
                            }
                            .foregroundColor(Color.init(hex: "0592BD"))
                            Spacer()
                        }
                        .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .topLeading)
                        //.background(Color.orange)
                        .padding()
                    }
                    .padding()
                    
                    Spacer()
                }
                .navigationBarHidden(true)
            }
            
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

/* Extension para utilizar colores en hexadecimal*/
extension Color {
    init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        
        self.init(
            .sRGB,
            red: Double(r) / 255,
            green: Double(g) / 255,
            blue:  Double(b) / 255,
            opacity: Double(a) / 255
        )
    }
}
