//
//  LogoView.swift
//  test-bankapp-ios-endcom
//
//  Created by Abraham Rubio on 18/06/21.
//

import SwiftUI

struct LogoView: View {
    var body: some View {
        HStack {
            Circle()
                .stroke(Color.init(hex: "07BE92"), lineWidth: 5)
                .frame(width: 25, height: 25)
            Text("Bankapp")
                .font(.title2)
                .foregroundColor(Color.init(hex: "07BE92"))
        }
        .padding(.bottom, 5)
    }
}

struct LogoView_Previews: PreviewProvider {
    static var previews: some View {
        LogoView()
    }
}
